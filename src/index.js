import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name ="John Smith";
// // <h1>Hello, {name}</h1> - JSX (JS XML)
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(user){
//   return user.firstName + ' ' + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>

// root.render(element);

/*if this commands not work or error
   npm init -y
   npx create-react-app react-app

then do following command
   npm init -y
   npm install create-react-app
   npx create-react-app react-app*/