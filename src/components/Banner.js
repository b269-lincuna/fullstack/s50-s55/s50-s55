// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';


/*
import { Button, Row, Col } from 'react-bootstrap';

export default function Banner() {
return (
    <Row>
        <Col className="p-5">
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <Button variant="primary">Enroll now!</Button>
        </Col>
    </Row>
    )
}
*/

import { Button, Row, Col } from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

export default function Banner({notFound}) {
    
    const navigate = useNavigate();

    function returnToHome(e){
        //e.preventDefault()
        navigate('/');
    }

    return (
        (notFound) ?
            <>
                <Row>
                    <Col className="p-5">
                        <h1>Error 404 - page not found.</h1>
                        <p>The page you are looking for cannot be found.</p>
                        <Button variant="primary" onClick={e => returnToHome(e)} >Back to Home</Button>
                    </Col>
                </Row>
            </>
        :
            <>   
                <Row>
                    <Col className="p-5">
                        <h1>Zuitt Coding Bootcamp</h1>
                        <p>Opportunities for everyone, everywhere.</p>
                        <Button variant="primary">Enroll now!</Button>
                    </Col>
                </Row>
            </>
    )
}

