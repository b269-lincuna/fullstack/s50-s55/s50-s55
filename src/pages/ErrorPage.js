import { Button, Row, Col } from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

export default function ErrorPage() {

	const navigate = useNavigate();

	function returnToHome(e){
	    e.preventDefault()
	    navigate('/');
	}

	return (
	    <Row>
	    	<Col className="p-5">
	            <h1>Error 404 - page not found.</h1>
	            <p>The page you are looking for cannot be found.</p>
	            <Button variant="primary" onClick={e => returnToHome(e)} >Back to Home</Button>
	        </Col>
	    </Row>
	)
}