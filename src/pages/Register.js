//https://react-bootstrap.github.io/forms/overview/
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';

import {Navigate , useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register() {

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNum, setMobileNum] = useState(0);
    
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine if submit will be disabled or not
    const [isActive, setIsActive] = useState(false);

    useEffect(()=>{
        if( (  email!=="" && password1!=="" && password2!=="" 
            && firstName!=="" && lastName!=="" && mobileNum!=="") 
            && (password1===password2) 
            && (mobileNum.length ===11)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email,password1,password2,firstName,lastName,mobileNum]);

    
    function registerUser (e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data) {
                console.log("email exist");
                Swal.fire({
                    title: "Email already in use.",
                    icon: "error",
                    text: "Please try different email."
                })
            } else {

                console.log("email not exist");
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        firstName : firstName,
                        lastName : lastName,
                        email : email,
                        mobileNo : mobileNum,
                        password : password1
                    })
                })
                .then(res2 => res2.json())
                .then(data2 => {
                    console.log(data2)

                    if(data2 === true) {
                        Swal.fire({
                            title: "Successfully Registered",
                            icon: "success",
                            text: "Your account is successfully registered."
                        })

                        navigate("/login")

                    } else {
                        Swal.fire({
                            title: "Registration Failed",
                            icon: "error",
                            text: "Please try again."
                        })
                    }

                })
            
            // Clear input fields
            setEmail("");
            setPassword1("");
            setPassword2("");
            setFirstName("");
            setLastName("");
            setMobileNum(0);
            }
            
        });
       
    }

    return (
        (user.id !== null) ?
            <Navigate to ="/courses" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter first name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter last name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNum">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter mobile number" 
                    value={mobileNum}
                    onChange={e => setMobileNum(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2" className="mb-2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>

            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )

}

